# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
specialist = Specialist.create([{name: 'Long,Young', specialty: 'Anesthesiology'},
                                {name: 'Austin, Steve', specialty: 'Surgery'},
                                {name: 'King, William', specialty: 'Psychology'}])


patient = Patient.create([{name: 'Bill, Apple',street_address: '11 supply rd'},
                          {name: 'Serge, Ibaka',street_address: '24 OKC rd'},
                          {name: 'Greg, Blue',street_address: '32 yellow st'},
                          {name: 'steve, Job',street_address: '142 done rd'},
                          {name: 'Oliver, Johns',street_address: '232 luck dr'}])

insurance = Insurance.create([{name: 'Health one' , street_address:'1234 left st' },
                              {name: 'Health two' , street_address:'456 right st' },
                              {name: 'Health three' , street_address:'789 upside st' },
                              {name: 'Health four' , street_address:'741 sky ave' },
                              {name: 'Health five' , street_address:'4 happy st' }])

appoint = Appointment.create([{specialist_id: 1, patient_id:1, complaint: "surgery", appointment_date: '2014 November 25', fee: 100},
                              {specialist_id: 2, patient_id:2, complaint: "foot", appointment_date: '2014 december 5', fee: 250},
                              {specialist_id: 3, patient_id:3, complaint: "crazy", appointment_date: '2014 december 25', fee: 150},
                              {specialist_id: 3, patient_id:4, complaint: "agoraphobia", appointment_date: '2014 november 8', fee: 245},
                              {specialist_id: 2, patient_id:5, complaint: "cancer", appointment_date: '2014 November 3', fee: 545},
                              {specialist_id: 1, patient_id:5, complaint: "test", appointment_date: '2014 November 2', fee: 200},
                              {specialist_id: 1, patient_id:4, complaint: "surgery", appointment_date: '2014 October 5', fee: 700},
                              {specialist_id: 2, patient_id:3, complaint: "hand", appointment_date: '2014 November 25', fee: 750},
                              {specialist_id: 3, patient_id:2, complaint: "apiphobia", appointment_date: '2014 November 15', fee: 45},
                              {specialist_id: 1, patient_id:1, complaint: "test", appointment_date: '2014 November 16', fee: 200}])
